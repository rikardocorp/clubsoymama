import Vue from 'vue'
import Vuex from 'vuex'
import * as Firebase from 'firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    navbarScroll: false,
    visibleNewSale: false,
    isLogged: false,
    isLoading: false,
    isLoadingModal: false,
    notification: {
      count: 0,
      content: ''
    },
    user: {
      role: '',
      data: '',
      date: '',
      time: '',
      isClient: false,
      isPuntoVenta: false
    }
  },
  getters: {
    arrayToObject: (state, getters) => (value) => {
      console.log('Array To OBJECT')
      let newArray = []
      value.forEach(function (value, index) {
        newArray.push({id: index, name: value})
      })
      return newArray
    }
  },
  mutations: {
    navbarScroolChange: (state, value) => {
      Vue.set(state, 'navbarScroll', value)
    },
    switchLoading: (state, value) => {
      console.log('SWITCH LOADING')
      Vue.set(state, 'isLoading', value)
    },
    sendNotification: (state, value) => {
      let data = {
        count: state.notification.count + 1,
        content: {
          status: value.status,
          data: {message: value.message, status: 'frontend', success: value.status, url: ''}
        }
      }
      Vue.set(state, 'notification', data)
    }
  },
  actions: {
    dispatchHTTP: ({commit, state, getters}, {type, url, data, notify = {success: false, error: false}}) => {
      console.log(type, url, data, notify)
      commit('switchLoading', true)
      let inquiry = ''
      let message = {}
      let keyStatus = ''
      let promise = new Promise((resolve, reject) => {
        let result = {
          status: false,
          content: []
        }
        switch (type) {
          case 'GET':
            inquiry = Vue.http.get(url)
            notify.success = false
            notify.error = true
            break
          case 'POST':
            inquiry = Vue.http.post(url, data)
            notify.success = true
            notify.error = true
            break
          case 'PATCH':
            inquiry = Vue.http.patch(url, data)
            notify.success = true
            notify.error = true
            break
          case 'DELETE':
            console.log('DELETE')
            console.log(url)
            console.log(data)
            inquiry = Vue.http.delete(url, {body: data})
            notify.success = true
            notify.error = true
            break
          case 'FILE':
            notify = {success: false, error: false}
            inquiry = Vue.http.get(url)
            break
          case 'LOAD_PDF':
            notify = {success: false, error: false}
            inquiry = Vue.http.get(url, {responseType: 'blob'})
            break
          default:
            inquiry = []
            result.status = null
            result.content = 'only [GET,INSERT,UPDATE,DELETE,FILE,LOAD_TABLE,LOAD_PDF]'
            resolve(result)
        }

        // SUCCSESS
        inquiry.then(response => {
          console.log('SUCCESSSSSSSSS 2424342434423')
          console.log(response)
          message.status = response.data.success
          message.data = {message: response.data.message, status: response.status, success: response.data.success, url: response.url}

          keyStatus = response.data.success ? 'success' : 'error'
          // if (notify[keyStatus]) {
          //   commit('pushNotification', message)
          // }
          result.status = response.data.success
          result.content = response.body.data ? response.body.data : response
          resolve(result)
        })

        // ERROR
        inquiry.catch(error => {
          console.log('ERRRRRORRRRRR')
          console.log(error)
          message.status = false
          message.data = {message: 'Ocurrio un problema inesperado, intenta nuevamente', status: error.status, success: false, url: error.url}
          // if (notify.error) {
          //   commit('pushNotification', message)
          // }
          result.content = error
          reject(result)
        })
      })
      promise.then(() => {
        commit('switchLoading', false)
      }).catch(() => {
        commit('switchLoading', false)
      })
      return promise
    }
  }
})
