import firebase from 'firebase'

export const fireDB = firebase.initializeApp({
  apiKey: 'AIzaSyDlWThieaPKjn6aD3lArNMiaEQgqf54lS4',
  authDomain: 'club-soymama.firebaseapp.com',
  databaseURL: 'https://club-soymama.firebaseio.com',
  projectId: 'club-soymama',
  storageBucket: 'club-soymama.appspot.com',
  messagingSenderId: '659594056964'
}).database()

export const fireSuscritos = fireDB.ref('suscritos')
