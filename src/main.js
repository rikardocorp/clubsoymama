// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRosource from 'vue-resource'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import store from './store/store'
import VueScroll from 'vue-scroll'
import VueScrollTo from 'vue-scrollto'
import VueFire from 'vuefire'
import Vuelidate from 'vuelidate'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
Vue.use(Notifications, { velocity })

Vue.use(Vuelidate)
// Vue.use(VueScrollTo)
Vue.use(VueScrollTo, {
  container: '#app',
  duration: 1500,
  easing: 'ease',
  offset: 0,
  cancelable: true,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})
Vue.use(VueFire)
Vue.use(VueScroll)
Vue.use(BootstrapVue)
Vue.use(VueRosource)
Vue.config.productionTip = false

Vue.http.options.root = 'http://127.0.0.1:8000/'
Vue.http.headers.common['Content-Type'] = 'application/json'
// Vue.http.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8080/'
// Vue.http.headers.common['Access-Control-Request-Method'] = '*'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
