import Vue from 'vue'
import Router from 'vue-router'
import WebPage from '@/components/WebPage'
import Admin from '@/components/Admin'
import Revistas from '@/components/admin/Revistas'
import Posts from '@/components/admin/Posts'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'webPage',
      component: WebPage
    },
    {
      path: '/admin',
      redirect: '/admin/revistas',
      name: 'admin',
      component: Admin,
      children: [
        {
          path: 'revistas',
          name: 'revistas',
          component: Revistas
          // meta: {requiresAuth: true, ROLE_ADMIN: true, ROLE_PUNTO_VENTA: true, ROLE_VENDEDOR: true}
        },
        {
          path: 'posts',
          name: 'posts',
          component: Posts
          // meta: {requiresAuth: true, ROLE_ADMIN: true, ROLE_PUNTO_VENTA: true, ROLE_VENDEDOR: true}
        }
      ]
    }
  ]
})
